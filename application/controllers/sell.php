<?php 

class Sell extends CI_Controller{
	
	public function __construct()
	{
		parent::__construct();		
		$this->load->helper("url");
		$this->load->model('m_sell');
	}

	public function index()
	{
	    $data['sell'] = $this->m_sell->sell();
		$this->load->view("V_SellMenu", $data);

	}

}