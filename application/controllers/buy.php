<?php 

class Buy extends CI_Controller{
	
	public function __construct()
	{
		parent::__construct();		
		$this->load->helper("url");
		$this->load->model('m_buy');
	}

	public function index()
	{
	    $data['buy'] = $this->m_buy->buy();
		$this->load->view("V_BuyMenu", $data);

	}

}