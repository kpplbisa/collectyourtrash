<!DOCTYPE html>
<html lang="">
	<head>
		<title>Login</title>
</head>
<body>
	<div class="container">
		<h3>Login</h3>
		<hr>
		<form action="<?php echo base_url('index.php/login/user') ?>" method="POST">
			<div class="form-group">
				<label for="cari">Username</label>
				<input name="username" type="text" autofocus required="required" class="form-control" id="username">
			</div>
			<div class="form-group">
				<label for="cari">Password</label>
				<input name="password" type="password" required="required" class="form-control" id="password">
			</div>
			<p>
			  <input class="btn btn-primary" type="submit" value="Login">
			  <input class="btn btn-primary" type="reset" value="Reset">
		  </p>
			<p><a href="index.php/signup">Belum punya akun? SignUp disini</a></p>
		</form>
		</div>
		</body>
		</html>


